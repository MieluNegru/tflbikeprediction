package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.utils.TestUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

public class TestStateGroupingAgent {

    private StateGroupingAgent stateGroupingAgent;

    DateTime now;
    DateTime yesterday;

    private List<BikeStationState> statesToGroup;
    private List<BikeStationState> expectedTodayStates;
    private List<BikeStationState> expectedYesterdayStates;

    @Before
    public void init() {
        stateGroupingAgent = new StateGroupingAgent();

        now = DateTime.now();
        yesterday = now.minusDays(1);

        // initialise the bike station state lists
        BikeStationState today1 = new TestUtils.BikeStationStateBuilder()
                .id(1)
                .dateTime(now)
                .build();
        BikeStationState today2 = new TestUtils.BikeStationStateBuilder()
                .id(1)
                .dateTime(now)
                .build();
        BikeStationState yesterday1 = new TestUtils.BikeStationStateBuilder()
                .id(1)
                .dateTime(yesterday)
                .build();
        BikeStationState yesterday2 = new TestUtils.BikeStationStateBuilder()
                .id(1)
                .dateTime(yesterday)
                .build();

        statesToGroup = Lists.newArrayList(today1, yesterday1, today2, yesterday2);
        expectedTodayStates = Lists.newArrayList(today1, today2);
        expectedYesterdayStates = Lists.newArrayList(yesterday1, yesterday2);
    }

    @Test
    public void testGroupStationStates() {
        Multimap<LocalDate, BikeStationState> groupedStates = stateGroupingAgent.groupStationStates(statesToGroup);
        LocalDate todayDate = now.toLocalDate();
        LocalDate yesterdayDate = yesterday.toLocalDate();

        Collection<BikeStationState> actualTodayStates = groupedStates.get(todayDate);

        Assert.assertEquals(expectedTodayStates, actualTodayStates);

        Collection<BikeStationState> actualYesterdayStates = groupedStates.get(yesterdayDate);
        Assert.assertEquals(expectedYesterdayStates, actualYesterdayStates);
    }
}