package com.tfl.bike.prediction.services.generator.naive;


import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimap;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.utils.TestUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

public class TestStateProgressAnalyzer {

    private StateProgressAnalyzer stateProgressAnalyzer = new StateProgressAnalyzer();

    @Test
    public void testCalculateGrow() {
        DateTime now = DateTime.now();
        LocalDate today = now.toLocalDate();

        BikeStationState state1 = new TestUtils.BikeStationStateBuilder()
                .dateTime(now)
                .bikesAvailable(5)
                .build();
        BikeStationState state2 = new TestUtils.BikeStationStateBuilder()
                .dateTime(now.minusMinutes(5))
                .bikesAvailable(3)
                .build();


        Multimap<LocalDate, BikeStationState> stateMap = ImmutableListMultimap.<LocalDate, BikeStationState>builder()
                .putAll(today, state1, state2)
                .build();
        int grow = stateProgressAnalyzer.calculateGrow(stateMap).get(0).getGrow();

        Assert.assertEquals(grow, 2);
    }

}