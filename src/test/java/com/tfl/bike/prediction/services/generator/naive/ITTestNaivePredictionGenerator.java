package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.collect.Lists;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import com.tfl.bike.prediction.domain.PredictedStationState;
import com.tfl.bike.prediction.utils.TestUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/context/context.xml"})
public class ITTestNaivePredictionGenerator {

    // id of station to make predictions on
    private final int stationId = 1;
    private final DateTime now = DateTime.now();
    private final int previousBikesAvailable = 7;

    private NaivePredictionContext predictionContext;

    @Before
    public void initPredictionContext() {
        PredictedStationState previousPrediction = initPreviousChunkState();
        List<BikeStationState> bikeStationStates = initBikeStationStates();
        PrecipitationMode currentPrecipitationMode = PrecipitationMode.NO;

        predictionContext = new NaivePredictionContext(previousPrediction, bikeStationStates, currentPrecipitationMode);
    }

    @Autowired
    private NaivePredictionGenerator naivePredictionGenerator;

    @Test
    public void testGeneratePrediction() {
        PredictedStationState generatedPrediction = naivePredictionGenerator.generatePrediction(now.plusMinutes(10),
                predictionContext);

        Assert.assertEquals(generatedPrediction.getId(), stationId);

        int expectedGrow = -1; // -4 grow on one day, +2 grow on the other. Average -1
        Assert.assertEquals(generatedPrediction.getAverageGrow(), expectedGrow);

        int growCount = 2; // 2 days used to determine the average grow
        Assert.assertEquals(generatedPrediction.getGrowCount(), 2);

        Assert.assertEquals(generatedPrediction.getPredictedBikesAvailable(), previousBikesAvailable + expectedGrow);
    }

    private PredictedStationState initPreviousChunkState() {
        return new PredictedStationState(stationId, now, previousBikesAvailable, 2, 4);
    }

    private List<BikeStationState> initBikeStationStates() {
        return Lists.newArrayList(
                // states from previous week
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(8)
                        .precipitation(PrecipitationMode.NO)
                        .dateTime(TestUtils.dateTimeFor("2014-11-16T15:00:00"))
                        .build(),
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(7)
                        .precipitation(PrecipitationMode.NO)
                        .dateTime(TestUtils.dateTimeFor("2014-11-16T15:10:00"))
                        .build(),
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(4)
                        .precipitation(PrecipitationMode.NO)
                        .dateTime(TestUtils.dateTimeFor("2014-11-16T15:21:00"))
                        .build(),
                // states from the week before. raining this week - states below should be ignored
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(5)
                        .precipitation(PrecipitationMode.RAIN)
                        .dateTime(TestUtils.dateTimeFor("2014-11-09T15:01:00"))
                        .build(),
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(5)
                        .precipitation(PrecipitationMode.RAIN)
                        .dateTime(TestUtils.dateTimeFor("2014-11-09T15:20:00"))
                        .build(),
                // and states from the week before that. Not raining this time, states below shouldn't be ignored
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(5)
                        .precipitation(PrecipitationMode.NO)
                        .dateTime(TestUtils.dateTimeFor("2014-11-02T15:05:00"))
                        .build(),
                new TestUtils.BikeStationStateBuilder()
                        .id(stationId)
                        .capacity(19)
                        .bikesAvailable(7)
                        .precipitation(PrecipitationMode.NO)
                        .dateTime(TestUtils.dateTimeFor("2014-11-02T15:25:00"))
                        .build()
        );
    }

}