package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.collect.Lists;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import com.tfl.bike.prediction.domain.PredictedStationState;
import com.tfl.bike.prediction.domain.exception.PredictionGenerationException;
import com.tfl.bike.prediction.utils.TestUtils;
import org.joda.time.DateTime;
import org.junit.Test;

public class TestContextValidator {

    private ContextValidator contextValidator = new ContextValidator();

    /**
     * Test that validation is successful on a context referring to one station only
     */
    @Test
    public void testValidatePass() {
        PredictedStationState prediction = new PredictedStationState(1, DateTime.now(), 0, 0, 0);

        BikeStationState state1 = new TestUtils.BikeStationStateBuilder().id(1).build();
        BikeStationState state2 = new TestUtils.BikeStationStateBuilder().id(1).build();

        NaivePredictionContext predictionContext = new NaivePredictionContext(
                prediction,
                Lists.newArrayList(state1, state2),
                PrecipitationMode.NO);

        // perform check. no exception should be thrown and test should pass
        contextValidator.validate(predictionContext);
    }

    /**
     * Test that validation fails on a list having BikeStationState entries with different station id's
     */
    @Test(expected = PredictionGenerationException.class)
    public void testValidateListFail() {
        PredictedStationState prediction = new PredictedStationState(1, DateTime.now(), 0, 0, 0);

        BikeStationState state1 = new TestUtils.BikeStationStateBuilder().id(1).build();
        BikeStationState state2 = new TestUtils.BikeStationStateBuilder().id(2).build(); // different id

        NaivePredictionContext predictionContext = new NaivePredictionContext(
                prediction,
                Lists.newArrayList(state1, state2),
                PrecipitationMode.NO);

        // perform check. should throw exception
        contextValidator.validate(predictionContext);
    }

    /**
     * Test that validation fails on a context with previous prediction having different station id
     * than the historical bike station states used in current prediction
     */
    @Test(expected = PredictionGenerationException.class)
    public void testValidatePreviousPredictionFail() {
        PredictedStationState prediction = new PredictedStationState(2, DateTime.now(), 0, 0, 0); // id=2

        BikeStationState state1 = new TestUtils.BikeStationStateBuilder().id(1).build(); // id=1
        BikeStationState state2 = new TestUtils.BikeStationStateBuilder().id(1).build(); // id=1

        NaivePredictionContext predictionContext = new NaivePredictionContext(
                prediction,
                Lists.newArrayList(state1, state2),
                PrecipitationMode.NO);

        // perform check. should throw exception
        contextValidator.validate(predictionContext);
    }

}