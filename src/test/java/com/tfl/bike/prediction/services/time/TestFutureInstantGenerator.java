package com.tfl.bike.prediction.services.time;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Unit test class for {@link com.tfl.bike.prediction.services.time.FutureInstantGenerator}
 * <p/>
 * Created by Gabriel on 11/18/2014.
 */
public class TestFutureInstantGenerator {

    private FutureInstantGenerator futureInstantGenerator;

    @Before
    public void init() {
        futureInstantGenerator = new FutureInstantGenerator();
    }

    @Test
    public void testGenerateInstants() {
        DateTime now = DateTime.now();
        DateTime expectedDateTime = now.withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(30);
        if(expectedDateTime.isBefore(now))
            expectedDateTime = expectedDateTime.plusMinutes(30);

        List<DateTime> generatedDateTimes = futureInstantGenerator.generateInstants(3, 30);
        for (int i = 0; i < 3; i++) {
            DateTime actualDateTime = generatedDateTimes.get(i);

            // check if the dates are equal. We are interested in minute precision
            Assert.assertEquals(expectedDateTime, actualDateTime);
            expectedDateTime = expectedDateTime.plusMinutes(30);
        }
    }

}
