package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.collect.Lists;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Unit test class for {@link StationStateFilter}
 * <p/>
 * Created by Gabriel on 11/22/2014.
 */
public class TestNaiveStationStateFilter {

    private StationStateFilter stationStateFilter;

    @Before
    public void init() {
        stationStateFilter = new StationStateFilter();
    }

    @Test
    public void testStationFilter() {
        BikeStationState rainState = new BikeStationState();
        rainState.setPrecipitation(PrecipitationMode.RAIN);

        BikeStationState noRainState = new BikeStationState();
        noRainState.setPrecipitation(PrecipitationMode.NO);

        List<BikeStationState> unfilteredStates = Lists.newArrayList(rainState, noRainState);

        List<BikeStationState> expectedFilteredStatesRain = Lists.newArrayList(rainState);
        List<BikeStationState> actualFilteredStatesRain = stationStateFilter.filterBikeStationStates(
                unfilteredStates, PrecipitationMode.RAIN);
        Assert.assertEquals(expectedFilteredStatesRain, actualFilteredStatesRain);

        List<BikeStationState> expectedFilteredStatesNoRain = Lists.newArrayList(noRainState);
        List<BikeStationState> actualFilteredStatesNoRain = stationStateFilter.filterBikeStationStates(
                unfilteredStates, PrecipitationMode.NO);
        Assert.assertEquals(expectedFilteredStatesNoRain, actualFilteredStatesNoRain);


    }

}
