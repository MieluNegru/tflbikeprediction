package com.tfl.bike.prediction.channels.database.output;

import com.tfl.bike.prediction.domain.PredictedStationState;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/context/context.xml"})
public class ITBikeStationPredictionJdbcTemplate {

    @Autowired
    private BikeStationPredictionJdbcTemplate stationPredictionJdbcTemplate;

    @Test
    public void testPredictionJdbcTemplate() {
        int id = 10;
        DateTime predictedTime = DateTime.now().plusYears(1);
        PredictedStationState prediction = new PredictedStationState(id, predictedTime, 10, 2, 3);

        if (stationPredictionJdbcTemplate.isPredictionStored(id, predictedTime)) {
            stationPredictionJdbcTemplate.deletePrediction(prediction);
        }

        stationPredictionJdbcTemplate.insertPrediction(prediction);
        Assert.assertTrue(stationPredictionJdbcTemplate.isPredictionStored(id ,predictedTime));

        stationPredictionJdbcTemplate.updatePrediction(prediction);
        Assert.assertTrue(stationPredictionJdbcTemplate.isPredictionStored(id ,predictedTime));

        stationPredictionJdbcTemplate.deletePrediction(prediction);
        Assert.assertFalse(stationPredictionJdbcTemplate.isPredictionStored(id ,predictedTime));
    }

}