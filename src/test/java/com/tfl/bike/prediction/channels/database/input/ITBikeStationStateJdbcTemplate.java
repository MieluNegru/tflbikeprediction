package com.tfl.bike.prediction.channels.database.input;

import com.tfl.bike.prediction.domain.BikeStationState;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Integration tests for {@link com.tfl.bike.prediction.channels.database.input.BikeStationStateJdbcTemplate}.
 * Check that database connection is successful and that methods for querying the database work correctly.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/context/context.xml"})
public class ITBikeStationStateJdbcTemplate {

    @Autowired
    private BikeStationStateJdbcTemplate bikeStationStateJdbcTemplate;

    /**
     * Test that BikeStationStateJdbcTemplate can retrieve ids of bike stations from database
     */
    @Test
    public void testGetStationIds() {
        List<Integer> stationIds = bikeStationStateJdbcTemplate.getStationIds();
        Assert.assertTrue(stationIds.size() > 0);
    }

    /**
     * Test that getBikeStationStates() retrieves the relevant historical states for making predictions.
     * Relevant states are states which occurred on the same day of week, between 30 and 0 minutes before
     * the prediction's time of day.
     */
    @Test
    public void testGetBikeStationStates() {
        // get the bike station states for a station id
        List<Integer> stationIds = bikeStationStateJdbcTemplate.getStationIds();
        int firstStationId = stationIds.get(0);
        DateTime now = DateTime.now();
        List<BikeStationState> bikeStationStates = bikeStationStateJdbcTemplate.getBikeStationStates(firstStationId, now);

        // now make sure that all the stations retrieved are relevant
        for (BikeStationState state : bikeStationStates) {
            Assert.assertTrue("Bike station state not relevant: " + state, isStateRelevant(state, now));
        }
    }

    private boolean isStateRelevant(BikeStationState state, DateTime predictionTime) {
        DateTime stateDateTime = state.getDateTime();

        // first check that the dates are in the same day of week
        int daysBetween = Days.daysBetween(stateDateTime, predictionTime).getDays();
        if (daysBetween % 7 != 0)
            return false;

        int minutesBetween = Minutes.minutesBetween(stateDateTime.plusDays(daysBetween), predictionTime).getMinutes();
        return minutesBetween <= 30;
    }
}















