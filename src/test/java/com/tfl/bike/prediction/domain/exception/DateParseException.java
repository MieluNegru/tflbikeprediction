package com.tfl.bike.prediction.domain.exception;

/**
 * Exception denoting that parsing a String into a {@link java.util.Date Date} failed
 *
 * Created by Gabriel on 11/23/2014.
 */
public class DateParseException extends RuntimeException {
    public DateParseException() {
    }
}
