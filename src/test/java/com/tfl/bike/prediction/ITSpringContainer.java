package com.tfl.bike.prediction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Integration test that makes sure that all the Spring beans have been correctly initialised and wired
 *
 * Created by Gabriel on 11/22/2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/context/context.xml"})
public class ITSpringContainer {

    @Autowired
    private ApplicationContext context;

    @Test
    public void testSpringContainer() {
        ClassPathXmlApplicationContext c;

        for(String name : context.getBeanDefinitionNames()) {
            System.out.println("Bean loaded in Spring context: " + name);
        }
    }
}
