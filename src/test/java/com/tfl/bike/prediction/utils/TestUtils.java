package com.tfl.bike.prediction.utils;

import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import com.tfl.bike.prediction.domain.exception.DateParseException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TestUtils contains static helper methods useful for performing routines
 * and initialisations commonly used in tests
 *
 * Created by Gabriel on 11/22/2014.
 */
public class TestUtils {

    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

    /**
     * Convenience method for creating a {@link org.joda.time.DateTime} from a date & time String
     * @param format date & time String in the format "yyyy-MM-ddTHH:mm:ss"; e.g. 2014-11-23T15:06:00
     * @return dateTime corresponding to the date & time String
     */
    public static DateTime dateTimeFor(String format) {
        return formatter.parseDateTime(format);
    }

    /**
     * Builder for {@link com.tfl.bike.prediction.domain.BikeStationState BikeStationState}
     */
    public static class BikeStationStateBuilder {
        private int id;
        private DateTime dateTime;
        private PrecipitationMode precipitation;
        private int bikesAvailable;
        private int capacity;

        public BikeStationStateBuilder id(int id) {
            this.id = id;
            return this;
        }

        public BikeStationStateBuilder dateTime(DateTime dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public BikeStationStateBuilder precipitation(PrecipitationMode precipitation) {
            this.precipitation = precipitation;
            return this;
        }

        public BikeStationStateBuilder bikesAvailable(int bikesAvailable) {
            this.bikesAvailable = bikesAvailable;
            return this;
        }

        public BikeStationStateBuilder capacity(int capacity) {
            this.capacity = capacity;
            return this;
        }

        public BikeStationState build() {
            BikeStationState state = new BikeStationState();

            state.setId(id);
            state.setDateTime(dateTime);
            state.setPrecipitation(precipitation);
            state.setBikesAvailable(bikesAvailable);
            state.setCapacity(capacity);

            return state;
        }
    }

}
