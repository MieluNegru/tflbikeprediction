package com.tfl.bike.prediction.channels.database.input;

import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Mapper class from database result set to {@link com.tfl.bike.prediction.domain.BikeStationState}
 *
 * Created by Gabriel on 11/16/2014.
 */
public class BikeStationStateMapper implements RowMapper<BikeStationState> {

    @Override
    public BikeStationState mapRow(ResultSet rs, int rowNum) throws SQLException {
        BikeStationState stationState = new BikeStationState();

        stationState.setId(rs.getInt("stationId"));

        stationState.setDateTime(new DateTime(rs.getTimestamp("stateDate").getTime()));

        stationState.setBikesAvailable(rs.getInt("bikeNumber"));
        stationState.setCapacity(rs.getInt("docksNumber"));

        String precipitationMode = rs.getString("precipitationMode");
        stationState.setPrecipitation(PrecipitationMode.fromName(precipitationMode));

        return stationState;
    }
}
