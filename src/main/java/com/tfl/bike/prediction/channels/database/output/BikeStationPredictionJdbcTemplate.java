package com.tfl.bike.prediction.channels.database.output;


import java.sql.Timestamp;
import java.util.Map;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


import com.google.common.collect.ImmutableMap;
import com.tfl.bike.prediction.domain.PredictedStationState;



/**
 * Implementation of
 * {@link com.tfl.bike.prediction.channels.database.output.BikeStationPredictionDao}
 * used by the naive prediction generator
 *
 * Created by Gabriel on 11/12/2014.
 */
public class BikeStationPredictionJdbcTemplate implements
		BikeStationPredictionDao<PredictedStationState> {

	private NamedParameterJdbcTemplate jdbcTemplate;

	public BikeStationPredictionJdbcTemplate(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}


	@Override
	public boolean isPredictionStored(int stationId, DateTime dateTime) {
		String sql = "SELECT count(*) FROM predictedState WHERE stationId = :stationId AND predictedTime = :predictedTime;";

		Map<String, Object> paramsMap = new ImmutableMap.Builder<String, Object>()
				.put("stationId", stationId)
				.put("predictedTime", new Timestamp(dateTime.getMillis()))
				.build();

		int cnt = jdbcTemplate.queryForObject(sql, paramsMap, Integer.class);
		return cnt > 0;
	}

	@Override
	public void insertPrediction(PredictedStationState prediction)
			throws DataIntegrityViolationException {
		String sql = "INSERT INTO predictedState(stationId, predictedTime, predictedBikesAvailable, avgGrow, growCount)\n" +
				"VALUES(:stationId, :predictedTime, :predictedBikesAvailable, :avgGrow, :growCount);";
		
		Map<String, Object> paramsMap = new ImmutableMap.Builder<String, Object>()
				.put("stationId", prediction.getId())
				.put("predictedTime", new Timestamp(prediction.getPredictedTime().getMillis()))
				.put("predictedBikesAvailable", prediction.getPredictedBikesAvailable())
				.put("avgGrow", prediction.getAverageGrow())
				.put("growCount", prediction.getGrowCount())
				.build();
		int rowsInserted = jdbcTemplate.update(sql, paramsMap);
	}

	@Override
	public void updatePrediction(PredictedStationState prediction)
			throws DataAccessException {
		String sql = "UPDATE predictedState SET\n" +
				"predictedBikesAvailable = :predictedBikesAvailable,\n" +
				"avgGrow = :avgGrow,\n" +
				"growCount = :growCount\n" +
				"WHERE stationId = :stationId\n" +
				"AND predictedTime = :predictedTime;";
		
		Map<String, Object> paramsMap = new ImmutableMap.Builder<String, Object>()
				.put("stationId", prediction.getId())
				.put("predictedTime", new Timestamp(prediction.getPredictedTime().getMillis()))
				.put("predictedBikesAvailable", prediction.getPredictedBikesAvailable())
				.put("avgGrow", prediction.getAverageGrow())
				.put("growCount", prediction.getGrowCount())
				.build();
		
		jdbcTemplate.update(sql, paramsMap);
	}

	@Override
	public void writePrediction(PredictedStationState prediction) {
		if (isPredictionStored(prediction.getId(),
				prediction.getPredictedTime()))
			updatePrediction(prediction);
		else
			insertPrediction(prediction);
	}

    @Override
    public void deletePrediction(PredictedStationState prediction) {
        String sql = "DELETE FROM predictedState WHERE stationId = :stationId AND predictedTime = :predictedTime";
        Map<String, Object> paramsMap = new ImmutableMap.Builder<String, Object>()
                .put("stationId", prediction.getId())
                .put("predictedTime", new Timestamp(prediction.getPredictedTime().getMillis()))
                .build();

        jdbcTemplate.update(sql, paramsMap);
    }

}
