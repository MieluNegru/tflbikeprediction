package com.tfl.bike.prediction.channels.database.input;

import com.google.common.collect.ImmutableMap;
import com.tfl.bike.prediction.domain.BikeStationState;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * BikeStationStateJdbcTemplate is the database reader implementation used retrieving entries
 * for the naive prediction generator.
 *
 * Created by Gabriel on 11/13/2014.
 */
public class BikeStationStateJdbcTemplate implements BikeStationStateDao<BikeStationState> {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private BikeStationStateMapper bikeStationStateMapper;

    public BikeStationStateJdbcTemplate(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        bikeStationStateMapper = new BikeStationStateMapper();
    }

    @Override
    public List<Integer> getStationIds() {
        String sql = "SELECT stationId FROM bikeStation;";
        return jdbcTemplate.queryForList(sql, new MapSqlParameterSource(), Integer.class);
    }

    /**
     * Retrieve all the previous states of a bike station, which have been registered on the
     * same day of previous weeks, at the specified time of the day.
     * <p/>
     * The timestamp is converted to UNIX timestamp. This UNIX timestamp is then used in the
     * query to retrieve only the required records. This is achieved by the following method:
     * <p/>
     * If currentTimestamp - recordTimestamp is roughly a multiple of WEEK_INTERVAL (with an
     * error of 30 min), then we are interested in the record and select it
     * <p/>
     * For example, if timestamp corresponds to 13:15:00 on a Thursday, then, we will retrieve
     * all the states on every Thursday between 12:45:00 and 13:15:00, for the station with
     * {@code stationId}
     *
     * @param stationId unique id of the station
     * @param dateTime  date and time for the prediction
     */
    @Override
    public List<BikeStationState> getBikeStationStates(int stationId, DateTime dateTime) {
        Timestamp timestamp = new Timestamp(dateTime.getMillis());

        String fatArseQuery = "SELECT stationId, stateDate, bikeNumber, docksNumber, precipitationMode\n" +
                "FROM (\n" +
                "    SELECT stationId, bikeNumber, docksNumber, precipitationMode,\n" +
                "    FROM_UNIXTIME(b.timestamp / 1000) AS stateDate, \n" +
                "    TIMESTAMPDIFF(DAY, (SELECT stateDate), :stateTimestamp) AS dayDiff \n" +
                "    FROM bikeStationState AS b \n" +
                "    INNER JOIN weatherState AS w \n" +
                "    ON b.timestamp = w.timestamp  \n" +
                "    WHERE stationId = :stationId \n" +
                "    HAVING dayDiff % 7 = 0 \n" +
                "    AND TIMESTAMPDIFF(MINUTE, DATE_ADD(stateDate, INTERVAL dayDiff DAY), :stateTimestamp) BETWEEN 0 AND 30 \n" +
                ") AS t1\n" +
                "ORDER BY stateDate ASC;";

        Map<String, Object> paramsMap = new ImmutableMap.Builder<String, Object>()
                .put("stationId", stationId)
                .put("stateTimestamp", timestamp)
                .build();

        return jdbcTemplate.query(fatArseQuery, paramsMap, bikeStationStateMapper);
    }

    @Override
    public BikeStationState getLastBikeStationState(int stationId) {
        String sql = "SELECT stationId, bikeNumber, docksNumber, precipitationMode,\n" +
                "FROM_UNIXTIME(b.timestamp / 1000) AS stateDate\n" +
                "FROM bikeStationState AS b\n" +
                "INNER JOIN weatherState AS w\n" +
                "ON b.timestamp = w.timestamp\n" +
                "ORDER BY b.timestamp DESC\n" +
                "LIMIT 1";
        Map<String, Object> paramsMap = new ImmutableMap.Builder<String, Object>()
                .put("stationId", stationId)
                .build();

        return jdbcTemplate.queryForObject(sql, paramsMap, bikeStationStateMapper);
    }
}
