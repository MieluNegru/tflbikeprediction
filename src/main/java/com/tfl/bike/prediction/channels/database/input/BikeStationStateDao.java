package com.tfl.bike.prediction.channels.database.input;

import com.tfl.bike.prediction.domain.BikeStationState;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.List;

/**
 * Interface for retrieving TFL bike station state information from the database
 * <p/>
 * Created by Gabriel on 11/7/2014.
 */
public interface BikeStationStateDao<T> {

    /**
     * Get unique ids of all the bike stations recorded in the database
     * @return unique ids of all the bike stations recorded in the database
     */
    List<Integer> getStationIds();

    /**
     * Get bike station states for station with specified id. Retrieve only
     * the relevant entries for making the prediction for the specified
     * timestamp
     *
     * @param stationId unique id of the station
     * @param dateTime  date and time for the prediction
     * @return states relevant for generating prediction for the specified timestamp
     */
    List<T> getBikeStationStates(int stationId, DateTime dateTime);

    /**
     * Get the most recent station state for stationId
     */
    BikeStationState getLastBikeStationState(int stationId);

}
