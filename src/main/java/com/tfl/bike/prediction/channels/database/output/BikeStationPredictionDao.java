package com.tfl.bike.prediction.channels.database.output;

import org.joda.time.DateTime;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;

import java.sql.Timestamp;

/**
 * Interface for persisting generated predictions of bike station usage to the database.
 * <p/>
 * Created by Gabriel on 11/12/2014.
 */
public interface BikeStationPredictionDao<T> {

    /**
     * Ascertain if a prediction with { stationId, timestamp } as the primary key is stored in the database.
     *
     * @param stationId the unique identifier of the station for which the prediction is made
     * @param dateTime the date and time corresponding to the date and time for which the prediction is made
     * @return if a prediction with { stationId, timestamp } as the primary key is stored in the database.
     */
    public boolean isPredictionStored(int stationId, DateTime dateTime);

    /**
     * Insert a new prediction in the database.
     *
     * @param prediction the new prediction to insert in the database.
     * @throws DataIntegrityViolationException if a record with the same primary key already exists
     *                                         in the predictions table. If you want to update an already existing prediction, use {@link #updatePrediction(Object) updatePrediction()}
     */
    public void insertPrediction(T prediction) throws DataIntegrityViolationException;

    /**
     * Update an existing prediction record in the database, i.e. a record having the same
     * primary key as the method's argument
     *
     * @param prediction the prediction to update in the dabase
     * @throws DataAccessException if an error occurs while updating the prediction record
     */
    public void updatePrediction(T prediction) throws DataAccessException;

    /**
     * Convenience method for writing a prediction record to the database.
     * <p/>
     * Inserts or updates prediction into the database. If a record with the prediction's
     * timestamp already exists in the database, this method updates it, instead of inserting
     * a new record
     *
     * @param prediction the prediction to persist in the database
     */
    public void writePrediction(T prediction);

    /**
     * Delete corresponding prediction row from database.
     * @param prediction the prediction to delete from the database
     */
    public void deletePrediction(T prediction);

}
