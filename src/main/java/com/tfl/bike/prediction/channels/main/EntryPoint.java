package com.tfl.bike.prediction.channels.main;

import com.tfl.bike.prediction.channels.database.input.BikeStationStateDao;
import com.tfl.bike.prediction.channels.database.input.BikeStationStateJdbcTemplate;
import com.tfl.bike.prediction.channels.database.output.BikeStationPredictionDao;
import com.tfl.bike.prediction.channels.database.output.BikeStationPredictionJdbcTemplate;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PredictedStationState;
import com.tfl.bike.prediction.services.generator.PredictionContext;
import com.tfl.bike.prediction.services.generator.PredictionGenerator;
import com.tfl.bike.prediction.services.generator.naive.NaivePredictionContext;
import com.tfl.bike.prediction.services.generator.naive.NaivePredictionGenerator;
import com.tfl.bike.prediction.services.time.FutureInstantGenerator;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Main class
 * <p/>
 * Created by Gabriel on 11/7/2014.
 */
public class EntryPoint {

    private static final long HALF_HOUR_INTERVAL = 30 * 60 * 1000;

    public static void main(String args[]) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context/context.xml");

        BikeStationStateDao<BikeStationState> inputDatabaseTemplate =
                context.getBean("inputDatabaseTemplate", BikeStationStateJdbcTemplate.class);

        BikeStationPredictionDao<PredictedStationState> outputDatabaseTemplate =
                context.getBean("outputDatabaseTemplate", BikeStationPredictionJdbcTemplate.class);

        NaivePredictionGenerator predictionGenerator = context.getBean("predictionGenerator", NaivePredictionGenerator.class);
        FutureInstantGenerator futureInstantGenerator = context.getBean("futureInstantGenerator", FutureInstantGenerator.class);

        List<Integer> stationIds = inputDatabaseTemplate.getStationIds();

        while (true) {
            List<DateTime> futureInstants = futureInstantGenerator.generateInstants(48, 30);
            for (int stationId : stationIds) {

                System.out.println(DateTime.now().toString() + " - Generating predictions for station id " + stationId);

                BikeStationState lastBikeStationState = inputDatabaseTemplate.getLastBikeStationState(stationId);
                PredictedStationState previousPrediction = new PredictedStationState(
                        stationId, lastBikeStationState.getDateTime(), lastBikeStationState.getBikesAvailable(), 0, 0);

                for (DateTime nextInstant : futureInstants) {
                    List<BikeStationState> bikeStationStates = inputDatabaseTemplate.getBikeStationStates(stationId, nextInstant);

                    NaivePredictionContext predictionContext = new NaivePredictionContext(
                            previousPrediction, bikeStationStates, lastBikeStationState.getPrecipitation());
                    PredictedStationState newPrediction = predictionGenerator.generatePrediction(nextInstant, predictionContext);

                    System.out.println(DateTime.now().toString() + " - Generated new prediction: " + newPrediction.toString());

                    outputDatabaseTemplate.writePrediction(newPrediction);
                    previousPrediction = newPrediction;
                }
            }
            try {
                Thread.sleep(HALF_HOUR_INTERVAL);
            } catch (InterruptedException e) {
                return;
            }
        }

    }

}
