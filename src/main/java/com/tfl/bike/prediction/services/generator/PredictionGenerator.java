package com.tfl.bike.prediction.services.generator;

import com.tfl.bike.prediction.domain.PredictedStationState;
import org.joda.time.DateTime;

/**
 * Interface responsible for generating predictions for bike station usage
 * <p/>
 * A prediction denotes how many bikes will be at the station at a particular time in the future.
 * <p/>
 * Created by Gabriel on 11/13/2014.
 */
public interface PredictionGenerator<T extends PredictionContext> {

    /**
     * Generate a new bike station usage prediction based on historical station states corresponding
     * to a bike station.
     *
     * @param predictionTime              the time for which the prediction is made
     * @param predictionContext           implementation-specific wrapper of data that a prediction
     *                                    generator needs. Examples would be a list of BikeStationStates,
     *                                    weather conditions, etc.
     * @return newly-generated prediction object
     */
    public PredictedStationState generatePrediction(DateTime predictionTime,
                                                    T predictionContext);

}
