package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.collect.Multimap;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.BikeStationStateProgress;
import org.joda.time.LocalDate;

import java.util.*;

/**
 * StateProgressAnalyzer is a bean responsible for calculating the variation in the number of bikes
 * in a bikestand (or bike station), a.k.a. the "grow".
 * <p/>
 * Positive grow denotes that the bike number has increased over the time, whereas negative grow
 * denotes that the number of bikes has decreased in the bike station.
 * <p/>
 * Created by Gabriel on 11/18/2014.
 */
public class StateProgressAnalyzer {

    // comparator used to sort lists of bike station states by their timestamp
    private static Comparator<BikeStationState> bikeStationStateComparator = new Comparator<BikeStationState>() {
        @Override
        public int compare(BikeStationState state1, BikeStationState state2) {
            return state1.getDateTime().compareTo(state2.getDateTime());
        }
    };

    /**
     * Compute the grow (variation of the number of bikes in the bikestand) for each entry in the
     * multimap.
     * <p/>
     * The multimap will contain for each day a list of entries corresponding to
     * {@link com.tfl.bike.prediction.domain.BikeStationState BikeStationState} recorded at different
     * times on that day. For each day, the grow is obtained by subtracting the first state's number of
     * bikes from the last state's number of bikes
     *
     * @param groupedStates bike station states grouped by the date on which they were recorded
     * @return list with the grows of the bike stations, one for each entry in {@code groupedStates}
     */
    public List<BikeStationStateProgress> calculateGrow(Multimap<LocalDate, BikeStationState> groupedStates) {
        List<BikeStationStateProgress> bikeStationStateProgressList = new ArrayList<>();

        // for each date, get the bike stations associated with it and determine the grow on that day
        for (LocalDate date: groupedStates.keySet()) {
            List<BikeStationState> bikeStationStates = new ArrayList<>(groupedStates.get(date));

            int listSize = bikeStationStates.size();
            if (listSize == 0) {
                continue;
            }

            // sort the bike stations by their timestamp
            Collections.sort(bikeStationStates, bikeStationStateComparator);

            // calculate the grow. The grow is the variation of the number of bikes between the first and last state
            BikeStationState firstState = bikeStationStates.get(0);
            BikeStationState lastState = bikeStationStates.get(listSize - 1);
            int grow = lastState.getBikesAvailable() - firstState.getBikesAvailable();

            BikeStationStateProgress progress = new BikeStationStateProgress(lastState, grow);
            bikeStationStateProgressList.add(progress);
        }

        return bikeStationStateProgressList;
    }

}
