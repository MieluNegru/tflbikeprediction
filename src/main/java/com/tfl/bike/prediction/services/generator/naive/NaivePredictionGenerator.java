package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.collect.Multimap;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.BikeStationStateProgress;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import com.tfl.bike.prediction.domain.PredictedStationState;
import com.tfl.bike.prediction.services.generator.PredictionGenerator;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Naive implementation of {@link com.tfl.bike.prediction.services.generator.PredictionGenerator PredictionGenerator}
 * simply relying on average of previous bike station state metrics, also considering weather factors
 * <p/>
 * Created by Gabriel on 11/13/2014.
 */
public class NaivePredictionGenerator implements PredictionGenerator<NaivePredictionContext> {

    @Autowired
    private ContextValidator contextValidator;

    @Autowired
    private StateGroupingAgent groupingAgent;

    @Autowired
    private StateProgressAnalyzer progressAnalyzer;

    @Autowired
    private StationStateFilter stateFilter;

    @Override
    public PredictedStationState generatePrediction(DateTime predictionTime,
                                                    NaivePredictionContext predictionContext) {

        // first get all the data in predictionContext for ease of access
        PredictedStationState previousTimeChunkPrediction = predictionContext.getPreviousTimeChunkPrediction();
        List<BikeStationState> bikeStationStates = predictionContext.getStationStates();
        PrecipitationMode currentPrecipitationMode = predictionContext.getCurrentPrecipitationMode();

        // first, perform initial check on bikeStationStates, to make sure they are all consistent
        contextValidator.validate(predictionContext);

        bikeStationStates = stateFilter.filterBikeStationStates(bikeStationStates, currentPrecipitationMode);

        Multimap<LocalDate, BikeStationState> groupedStates = groupingAgent.groupStationStates(bikeStationStates);
        List<BikeStationStateProgress> stateProgresses = progressAnalyzer.calculateGrow(groupedStates);

        int averageGrow = computeAverageGrow(stateProgresses);
        int predictedBikesAvailable = previousTimeChunkPrediction.getPredictedBikesAvailable() + averageGrow;
        int growCount = stateProgresses.size();

        return new PredictedStationState(
                previousTimeChunkPrediction.getId(), predictionTime, predictedBikesAvailable, averageGrow, growCount);
    }

    private int computeAverageGrow(List<BikeStationStateProgress> progresses) {
        int listSize = progresses.size();
        int growSum = 0;

        for (BikeStationStateProgress progress : progresses)
            growSum += progress.getGrow();

        return growSum / listSize;
    }

}
