package com.tfl.bike.prediction.services.generator.naive;

import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;
import com.tfl.bike.prediction.domain.PredictedStationState;
import com.tfl.bike.prediction.services.generator.PredictionContext;

import java.util.List;

/**
 * {@link com.tfl.bike.prediction.services.generator.PredictionContext PredictionContext} implementation
 * for the naive prediction generator.
 * <p/>
 * Relevant information is
 * <ul>
 * <li>the prediction made for the previous time chunk</li>
 * <li>the historical data regarding station states</li>
 * <li>the precipitation mode corresponding to the current (i.e. present) weather</li>
 * </ul>
 * <p/>
 * Created by Gabriel on 11/23/2014.
 */
public class NaivePredictionContext implements PredictionContext {
    private PredictedStationState previousTimeChunkPrediction;
    private List<BikeStationState> stationStates;
    private PrecipitationMode currentPrecipitationMode;

    /**
     * @param previousTimeChunkPrediction the prediction generated for the previous time portion. For
     *                                    example, when generating prediction for bike usage at time
     *                                    12:30, the prediction made for the usage at 12:00 is relevant.
     * @param stationStates               states acquired from historical data, which will be used for
     *                                    making predictions for a particular bike stand. All station
     *                                    states must refer to the same station (i.e. have the same
     *                                    stationId), which should also be the same as the id of the
     *                                    station used for the previousTimeChunkPrediction (previous
     *                                    parameter)
     * @param currentPrecipitationMode    precipitation mode corresponding to current weather data
     */
    public NaivePredictionContext(PredictedStationState previousTimeChunkPrediction,
                                  List<BikeStationState> stationStates,
                                  PrecipitationMode currentPrecipitationMode) {
        this.previousTimeChunkPrediction = previousTimeChunkPrediction;
        this.stationStates = stationStates;
        this.currentPrecipitationMode = currentPrecipitationMode;
    }

    /**
     * @return the prediction generated for the previous time portion. For example, when generating
     * prediction for bike usage at time 12:30, the prediction made for the usage at 12:00 is relevant.
     */
    public PredictedStationState getPreviousTimeChunkPrediction() {
        return previousTimeChunkPrediction;
    }

    /**
     * @return states acquired from historical data, which will be used for making predictions for a
     * particular bike stand
     */
    public List<BikeStationState> getStationStates() {
        return stationStates;
    }

    /**
     * @return precipitation mode corresponding to current weather data
     */
    public PrecipitationMode getCurrentPrecipitationMode() {
        return currentPrecipitationMode;
    }
}
