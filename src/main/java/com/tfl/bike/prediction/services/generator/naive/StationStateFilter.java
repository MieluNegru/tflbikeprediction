package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.PrecipitationMode;

import java.util.List;

/**
 * NaiveStationStateFilter is responsible for filtering queried bike station states. The
 * gist is that the data mining process is interested in only a subset of the returned
 * bike station states. This class's purpose is to separate this relevant subset from
 * the rest.
 * <p/>
 * The separation is done based on the weather information, namely, whether it's raining
 * or not. If current weather indicates rain, then this class filters only the bike
 * station states corresponding to raining weather.
 * <p/>
 * Created by Gabriel on 11/18/2014.
 */
public class StationStateFilter {

    /**
     * Filter list of bike station states by comparing their precipitation mode with that of the
     * current state. Return only the states with the same precipitation mode as the current state.
     *
     * @param bikeStationStates list of states to filter
     * @param precipitationMode all filtered bike station states will have this precipitation mode
     * @return filtered list of bike station states
     */
    public List<BikeStationState> filterBikeStationStates(List<BikeStationState> bikeStationStates, final PrecipitationMode precipitationMode) {
        Iterable<BikeStationState> bikeStationStateIterable;
        bikeStationStateIterable = Iterables.filter(bikeStationStates, new Predicate<BikeStationState>() {
            @Override
            public boolean apply(BikeStationState input) {
                return input.getPrecipitation() == precipitationMode;
            }
        });

        return Lists.newArrayList(bikeStationStateIterable);
    }

}
