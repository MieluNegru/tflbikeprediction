package com.tfl.bike.prediction.services.generator;

/**
 * PredictionContext is a wrapper around information that the prediction generator might need.
 * The nature of this information depends of the
 * {@link com.tfl.bike.prediction.services.generator.PredictionGenerator PredictionGenerator}
 * implementation. As a consequence, each implementation of PredictionGenerator should also
 * implement PredictionContext to hold the information it needs to generate the predictions.
 *
 * Created by Gabriel on 11/23/2014.
 */
public interface PredictionContext {
}
