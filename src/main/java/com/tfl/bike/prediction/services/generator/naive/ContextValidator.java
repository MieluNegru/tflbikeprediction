package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.tfl.bike.prediction.domain.BikeStationState;
import com.tfl.bike.prediction.domain.exception.PredictionGenerationException;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * ContextValidator is the first component in the naive prediction generator chain. Its purpose is to
 * validate the {@link com.tfl.bike.prediction.services.generator.naive.NaivePredictionContext PredictionContext},
 * that is, check if it is consistent. Specifically, it checks to see if all states refer to the same
 * station.
 *
 * Created by Gabriel on 11/22/2014.
 */
public class ContextValidator {

    public void validate(NaivePredictionContext predictionContext) {
        List<BikeStationState> bikeStationStates = predictionContext.getStationStates();

        // the list must have some items. otherwise, there is no average to calculate
        if (bikeStationStates.size() == 0) {
            throw new IllegalArgumentException("bikeStationStates argument can not have 0 length");
        }

        // get the id's of all items in the list. If there are 2 distinct id's, throw exception
        Collection<Integer> stateIds = Collections2.transform(bikeStationStates, new Function<BikeStationState, Integer>() {
            @Override
            public Integer apply(BikeStationState input) {
                return input.getId();
            }
        });
        Set<Integer> distinctSetIds = new HashSet<>(stateIds);

        if (distinctSetIds.size() != 1) {
            throw new PredictionGenerationException("Station states corresponding to different stations found. Station id's present: " + distinctSetIds.toString());
        }

        int previousId = predictionContext.getPreviousTimeChunkPrediction().getId();
        if (distinctSetIds.iterator().next() != previousId) {
            throw new PredictionGenerationException(
                    "Prediction over previous chunk has id " + previousId + "; not all bike station states have the same station id. Station id's present: " + distinctSetIds.toString());
        }
    }

}
