package com.tfl.bike.prediction.services.time;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * FutureInstantGenerator is responsible for generating a list of future instants in time, for which we
 * will make predictions.
 * <p/>
 * All timestamps have a constant interval between them
 * <p/>
 * Created by Gabriel on 11/18/2014.
 */
public class FutureInstantGenerator {

    /**
     * Create a list of DateTime's in the future, for which we will make predictions.
     * <p/>
     * Successive generated DateTime's have a constant span between each other. The first one
     * starts at the closest 0 min or 30 min mark in the future.
     * <p/>
     *
     * @param numberOfInstants the number of DateTime instances to generate.
     * @param minuteSpan       the number of minutes between each DateTime instance's timestamp
     */
    public List<DateTime> generateInstants(int numberOfInstants, int minuteSpan) {
        if (numberOfInstants <= 0 || minuteSpan <= 0) {
            throw new IllegalArgumentException("Error: numberOfInstants and minuteSpan must be strictly positive");
        }

        List<DateTime> generatedList = new ArrayList<>();

        DateTime now = new DateTime();
        DateTime currentEntry = now.withMillisOfSecond(0).withSecondOfMinute(0).withMinuteOfHour(30);
        if (currentEntry.isBefore(now))
            currentEntry = currentEntry.plusMinutes(30);

        for (int i = 0; i < numberOfInstants; i++) {
            generatedList.add(currentEntry);
            currentEntry = currentEntry.plusMinutes(minuteSpan);
        }

        return generatedList;
    }

}
