package com.tfl.bike.prediction.services.generator.naive;

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.tfl.bike.prediction.domain.BikeStationState;
import org.joda.time.LocalDate;

import java.util.List;

/**
 * Class responsible for grouping a list of {@link com.tfl.bike.prediction.domain.BikeStationState BikeStationStates}
 * into multiple sub-lists, each sub-list having all the states from one particular day
 *
 * Created by Gabriel on 11/18/2014.
 */
public class StateGroupingAgent {

    // the function applied when performing the grouping
    private static Function<BikeStationState, LocalDate> groupFunction = new Function<BikeStationState, LocalDate>() {
        @Override
        public LocalDate apply(BikeStationState input) {
            return input.getDateTime().toLocalDate();
        }
    };

    /**
     * Group bike station states by the day of their timestamp.
     *
     * @param bikeStationStates the states to group
     * @return multimap with the date of the station states as the key and station states
     *         on that day as the value
     */
    public Multimap<LocalDate, BikeStationState> groupStationStates(List<BikeStationState> bikeStationStates) {
        return Multimaps.index(bikeStationStates, groupFunction);
    }

}
