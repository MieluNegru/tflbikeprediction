package com.tfl.bike.prediction.domain;

import org.joda.time.DateTime;

import java.sql.Timestamp;

/**
 * Bean encapsulating the data corresponding to a bike station state relevant for data mining
 * <p/>
 * Created by Gabriel on 11/17/2014.
 */
public class BikeStationState {

    // unique id of the bike station
    private int id;
    // the date and time of the state
    private DateTime dateTime;
    // how many bikes are available at the bikestand
    private int bikesAvailable;
    // total number of slots at a bikestand
    private int capacity;
    // rain state
    private PrecipitationMode precipitation;

    public BikeStationState() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public int getBikesAvailable() {
        return bikesAvailable;
    }

    public void setBikesAvailable(int bikesAvailable) {
        this.bikesAvailable = bikesAvailable;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public PrecipitationMode getPrecipitation() {
        return precipitation;
    }

    public void setPrecipitation(PrecipitationMode precipitation) {
        this.precipitation = precipitation;
    }
}
