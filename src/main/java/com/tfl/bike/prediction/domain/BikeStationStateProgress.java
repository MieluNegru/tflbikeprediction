package com.tfl.bike.prediction.domain;

import java.sql.Timestamp;

/**
 * Bean modelling how the number of bikes of a station has changed over time
 *
 * Created by Gabriel on 11/13/2014.
 */
public class BikeStationStateProgress {

    // the state at the end of the time chunk on which we track the progress
    private BikeStationState endState;
    // the increase of the number of bikes. negative value means that there are
    // less bikes than at the beginning
    private int grow;

    public BikeStationStateProgress(BikeStationState endState, int grow) {
        this.endState = endState;
        this.grow = grow;
    }

    public BikeStationState getEndState() {
        return endState;
    }

    public int getGrow() {
        return grow;
    }
    
}
