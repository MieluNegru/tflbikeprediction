package com.tfl.bike.prediction.domain;

/**
 * Enum used to mark different precipitation states for weather
 *
 * Created by Gabriel on 11/16/2014.
 */
public enum PrecipitationMode {
    NO, RAIN;

    public static PrecipitationMode fromName(String name) {
        return name.toLowerCase().equals("no") ? NO : RAIN;
    }
}
