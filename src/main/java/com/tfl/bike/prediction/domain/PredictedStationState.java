package com.tfl.bike.prediction.domain;

import org.joda.time.DateTime;

/**
 * Class containing generated predictions resulted from intense data mining process (buzzword alert)
 * <p/>
 * Created by Gabriel on 11/13/2014.
 */
public class PredictedStationState {

    // unique id of the station for which the prediction is made
    private int id;
    // the time for which this prediction is made
    private DateTime predictedTime;
    // what the number of bikes will be
    private int predictedBikesAvailable;
    // how much the number of bikes available will have changed
    private int averageGrow;
    // number of entries used when calculating the average grow
    private int growCount;

    public PredictedStationState(int id, DateTime predictedTime, int predictedBikesAvailable, int averageGrow, int growCount) {
        this.id = id;
        this.predictedTime = predictedTime;
        this.predictedBikesAvailable = predictedBikesAvailable;
        this.averageGrow = averageGrow;
        this.growCount = growCount;
    }

    public int getId() {
        return id;
    }

    public DateTime getPredictedTime() {
        return predictedTime;
    }

    public int getPredictedBikesAvailable() {
        return predictedBikesAvailable;
    }

    public int getAverageGrow() {
        return averageGrow;
    }

    public int getGrowCount() {
        return growCount;
    }

    @Override
    public String toString() {
        return "PredictedStationState{" +
                "id=" + id +
                ", predictedTime=" + predictedTime +
                ", predictedBikesAvailable=" + predictedBikesAvailable +
                ", averageGrow=" + averageGrow +
                ", growCount=" + growCount +
                '}';
    }
}
