package com.tfl.bike.prediction.domain.exception;

/**
 * A PredictionGenerationException denotes, on a generic level, that something went wrong when generating
 * predictions on a set of {@link com.tfl.bike.prediction.domain.BikeStationState BikeStationState} items
 *
 * Created by Gabriel on 11/22/2014.
 */
public class PredictionGenerationException extends RuntimeException {

    public PredictionGenerationException(String message) {
        super(message);
    }
}
